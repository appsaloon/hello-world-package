<?php

require_once __DIR__ . '/vendor/autoload.php';
use as_packages\hello_world_package\Hello_World;

( new Hello_World() )->echo_hello_name( php_uname( 'n' ) );
