<?php

namespace as_packages\hello_world_package;

/**
 *
 */
class Hello_World {
	/**
	 * @param string $name
	 */
	public function echo_hello_name( string $name = 'world' ) {
		echo 'Hello ' . $name . PHP_EOL;
	}
}
